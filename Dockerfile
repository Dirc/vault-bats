
ARG VAULT_VERSION=1.8.4
ARG BATS_VERSION=v1.4.1

# FROM bats/bats:$BATS_VERSION AS bats-core-base

# FROM vault:$VAULT_VERSION
FROM vault:$VAULT_VERSION AS vault-base

FROM bats/bats:$BATS_VERSION

RUN apk update && apk add --quiet --no-cache \
    bash=5.0.17-r0

COPY --from=vault-base /bin/vault /usr/local/bin/vault
COPY --from=vault-base /vault /vault

#WORKDIR /code/

RUN vault --version >> version.txt && bats --version >> version.txt

ENTRYPOINT ["bash", "-c"]

CMD ["cat version.txt"]