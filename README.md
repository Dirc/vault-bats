
# Vault + BATS image to test policies

When you run Vault and need to design/modify your policies. You want to try them out, experiment and ensure everything keeps on working like you expect.
This made me look around for some simple testing mechanism. So you can create a test set and very each policy change against that set.
We choose [Bats](https://bats-core.readthedocs.io/en/latest/index.html) for this, which provides a simple and clean way to test the retun status of vault cli commands.

Image contains:

- [Vault](https://www.vaultproject.io/)
- [Bats](https://bats-core.readthedocs.io/en/latest/index.html)

## Usage

### Run tests

You can run the image by providing your policy file and your bats test file.

The container will start a vault server, apply the provided policy and create and use a token based on that policy for the bats tests.

```shell
# Docker
docker run -v "${PWD}:/code" dirc/vault-bats:latest policy.hcl test.bats

```

### Check UI manually

[Start a Vault server](./README_UI.md) with the same policy to verify the permissions in the UI.

### Create secrets, enable secret engines

If you need to create secrets or enable secret engines you can either:

- If your policy allows it, create/enable them in your bats test.
- If your policy does not allow this and you need the root token, modify the `tearup.bash`.

## Build

```shell

TAG=0.2
IMAGE=dirc/vault-bats

docker build -t $IMAGE:$TAG .
docker build -t $IMAGE:latest .

docker push $IMAGE:$TAG
docker push $IMAGE:latest

```

## ToDo

- [ ] no vault server output
- [ ] add `version` argument which returns vault and bats version
- [ ] add argument for custom `tearup.bash`
- [ ] make Docker image more lean
