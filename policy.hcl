path "secret/*" {
  capabilities = ["create", "update", "list", "delete"]
}
path "secret/metadata/*" {
  capabilities = ["read", "list"]
}