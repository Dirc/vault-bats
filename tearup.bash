#!/usr/bin/env bash

echo "######## START TEARUP ########"

POLICY_FILE=$1
POLICY_NAME=${POLICY_FILE%%.*}

vault server -dev -dev-root-token-id root & > /dev/null
sleep 1
export VAULT_ADDR=http://127.0.0.1:8200
export VAULT_TOKEN=root

# Create Policy and token
vault policy write $POLICY_NAME $POLICY_FILE

vault policy list
vault policy read $POLICY_NAME

vault token create -policy=$POLICY_NAME -format=json > /vault/vault-dev-token.json

export VAULT_TOKEN=$(cat /vault/vault-dev-token.json | jq -r '.auth.client_token')

vault kv put secret/foo bar=baz
vault kv put secret/foo bar=bazbaz

echo "######## FINISHED TEARUP ########"
echo
echo "######## READY FOR TESTS ########"
