#!/usr/bin/env bats

@test "list k/v paths" {
  vault kv list secret
}

@test "don't list k/v key" {
  run vault kv list secret/foo
  [ "$status" -eq 2 ]
}

@test "get k/v metadata" {
  vault kv metadata get secret/foo
}

@test "don't read secret" {
  run vault kv get secret/foo
  [ "$status" -eq 2 ]
}